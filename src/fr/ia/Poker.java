package fr.ia;

import fr.ia.dao.HandsDAO;
import fr.ia.job.Game;

import java.io.IOException;
import java.util.Iterator;

public class Poker
{
   static int numberOfwinGamerOne = 0;

    public static void main(String[] args) throws IOException
    {

        generateGame();

    }



    private static void generateGame()
    {


        HandsDAO dao = new HandsDAO();
        Iterator itr = dao.getListGame().iterator();
        while (itr.hasNext())
        {
            Game game = new Game(itr.next().toString());
            game.lunchGame();
            countWinner(game.getWinnerNumber());
        }
        System.out.println("");
        System.out.println("#########################################");
        System.out.println("#########################################");
        System.out.println("Le joueur 1 a gagné :" + numberOfwinGamerOne+" fois");
    }

    private static void countWinner(int winnerNumber)
    {
        if (winnerNumber == 1)
        {
            numberOfwinGamerOne++;
        }
    }


}
