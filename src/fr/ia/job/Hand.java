package fr.ia.job;

import java.util.ArrayList;

public class Hand
{

    /**
     * @author IOOSSEN AURELIEN created on 04/11/2019
     */
    private ArrayList<Card> listCard;

    public Hand() {
        listCard = new ArrayList<>();
    }

    public ArrayList<Card> getListCard()
    {
        return listCard;
    }

    public void setListCard(ArrayList<Card> listCard)
    {
        this.listCard = listCard;
    }

    public void addCard(Card newCard)
    {

        listCard.add(newCard);


    }

    public void removeCard(Card card)
    {
        listCard.remove(card);

    }

    @Override
    public String toString() {
        return "Hand{" +
                "listCard=" + listCard +
                "}";
    }
}
