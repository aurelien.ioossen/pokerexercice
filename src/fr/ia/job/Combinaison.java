package fr.ia.job;

public class Combinaison
{

    /**
     *  @author IOOSSEN AURELIEN created on 05/11/2019 
     *
     */

    private String combinaisonName;
    private int rank;
    private int highestValue;


    public Combinaison(String combinaisonName, int rank,int maxValue)
    {
        this.combinaisonName = combinaisonName;
        this.rank = rank;
        this.highestValue = maxValue;
    }


    public String getCombinaisonName()
    {
        return combinaisonName;
    }

    public void setCombinaisonName(String combinaisonName)
    {
        this.combinaisonName = combinaisonName;
    }

    public int getRank()
    {
        return rank;
    }

    public void setRank(int rank)
    {
        this.rank = rank;
    }

    public int getHighestValue()
    {
        return highestValue;
    }
}
