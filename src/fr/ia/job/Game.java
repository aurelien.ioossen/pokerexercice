package fr.ia.job;

import fr.ia.dao.HandsDAO;
import fr.ia.utils.HandEvaluator;
import fr.ia.utils.HandGenerator;

public class Game {
    private String StringGameInPokerFile;
    private int winnerNumber;
    private HandGenerator handGenerator;
    private HandEvaluator evaluatorhandOne;
    private HandEvaluator evaluatorhandTwo;

    public Game(String StringGameInPokerFile) {
        this.StringGameInPokerFile = StringGameInPokerFile;

    }

    public void lunchGame() {
        generateGamersHand(StringGameInPokerFile);
        evaluateHands();
        setWinnerOfGame();

    }

    private void generateGamersHand(String stringGameInPokerFile) {
        handGenerator = new HandGenerator(stringGameInPokerFile);
    }

    private void evaluateHands() {
        evaluatorhandOne = new HandEvaluator(handGenerator.getHandGamerOne(), 1);
        evaluatorhandTwo = new HandEvaluator(handGenerator.getHandGamerTwo(), 2);

    }

    private void setWinnerOfGame() {
        int result = evaluatorhandOne.compareTo(evaluatorhandTwo);
        if (result > 0) {
            winnerNumber = 1;
            System.out.println("JOUEUR 1 GAGANANT");
        } else {
            winnerNumber = 2;
            System.out.println("JOUEUR 2 GAGNANT");
        }

    }

    public int getWinnerNumber() {
        return winnerNumber;
    }
}
