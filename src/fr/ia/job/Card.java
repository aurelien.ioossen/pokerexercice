package fr.ia.job;

public class Card implements Comparable
{

    /**
     * @author IOOSSEN AURELIEN created on 04/11/2019
     */

    private Integer cardValue;
    private Enum cardColor;


    public Card(Character cardValueChar, Character cardColorChar)
    {
        setCardValue(cardValueChar);
        setCardColor(cardColorChar);
    }

    public Integer getCardValue()
    {
        return cardValue;
    }

    private void setCardValue(Character cardValueChar)
    {
        Integer value;

        switch (cardValueChar)
        {

            case 'T':
                value = 10;
                break;

            case 'J':
                value = 11;
                break;

            case 'Q':
                value = 12;
                break;

            case 'K':
                value = 13;
                break;

            case 'A':
                value = 14;
                break;

            default:
                value = Integer.parseInt(String.valueOf(cardValueChar));


        }
        cardValue = value;
    }

    public Enum getCardColor()
    {
        return cardColor;
    }

    private void setCardColor(Character cardValueChar)
    {
        switch (cardValueChar)
        {

            case 'H':
                cardColor = Color.COEUR;
                break;

            case 'D':
                cardColor = Color.CARREAU;
                break;

            case 'C':
                cardColor = Color.TREFLE;
                break;

            case 'S':
                cardColor = Color.PIQUE;
                break;

        }
    }

    @Override
    public String toString()
    {
        return "Card{" + "cardValue=" + cardValue + ", cardColor=" + cardColor + '}';
    }


    @Override
    public int compareTo(Object o)
    {
        Card other = (Card) o;
        return cardValue - other.getCardValue();
    }
}
