package fr.ia.dao;

import fr.ia.job.Card;
import fr.ia.job.Hand;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public final class HandsDAO {

    /**
     * @author IOOSSEN AURELIEN created on 04/11/2019
     */

    private BufferedReader pokerfile;
    private ArrayList<String> listGame;


    public HandsDAO() {
        listGame = new ArrayList<>();
        try {
            String line;
            ClassLoader classLoader = getClass().getClassLoader();
            pokerfile = new BufferedReader(new FileReader(classLoader.getResource("poker.txt").getFile()));

            while ((line = pokerfile.readLine()) != null) {
                listGame.add(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<String> getListGame() {
        return listGame;
    }
}
