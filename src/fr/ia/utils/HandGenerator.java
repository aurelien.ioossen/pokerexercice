package fr.ia.utils;

import fr.ia.job.Card;
import fr.ia.job.Hand;

public class HandGenerator
{

    /**
     * @author IOOSSEN AURELIEN created on 05/11/2019
     */

    private Hand handGamerOne;
    private Hand handGamerTwo;
    private String StringGameInPokerFile;

    public HandGenerator(String StringGameInPokerFile)
    {
        this.StringGameInPokerFile = StringGameInPokerFile;
        handGamerOne = new Hand();
        handGamerTwo = new Hand();

        generateGamersHand();
    }


    public Hand getHandGamerOne()
    {
        return handGamerOne;
    }

    public Hand getHandGamerTwo()
    {
        return handGamerTwo;
    }

    private void generateGamersHand()
    {
        System.out.println("");
        System.out.println("#############################################");
        System.out.println("GENERATION DES MAINS - CARTES :" + StringGameInPokerFile);

        int curseurDepart = 0;

        for (int nmbreSeparator = 0; nmbreSeparator < 10; nmbreSeparator++)
        {
            char cardValue = StringGameInPokerFile.charAt(curseurDepart);
            char cardColor = StringGameInPokerFile.charAt((++curseurDepart));
            Card card = new Card(cardValue, cardColor);
            if (nmbreSeparator < 5)
            {
                handGamerOne.addCard(card);
            }
            else
            {
                handGamerTwo.addCard(card);
            }
            curseurDepart += 2;

        }

        System.out.println("Joueur 1 :" + handGamerOne.toString());
        System.out.println("Joueur 2 :" + handGamerTwo.toString());


    }
}
