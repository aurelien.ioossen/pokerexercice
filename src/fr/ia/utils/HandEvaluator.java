package fr.ia.utils;

import fr.ia.job.Card;
import fr.ia.job.Combinaison;
import fr.ia.job.Hand;

import java.util.ArrayList;
import java.util.HashMap;

public class HandEvaluator implements Comparable {

    ArrayList<Card> cardList;
    Combinaison combinaison;
    private int valueMaxOccurenced;
    private int numberOfGamer;


    public HandEvaluator(Hand handGamer, int numberOfGamer) {
        this.cardList = handGamer.getListCard();
        cardList.sort(Card::compareTo);
        this.numberOfGamer = numberOfGamer;
        determinateNumberOfOccurence();
        determinateHandRank();
        afficherResult();

    }

    private void afficherResult() {
        System.out.println("\nJOUEUR N°" + numberOfGamer);
        System.out.println("OCCURENCES :" + determinateNumberOfOccurence());
        System.out.println("COMBINAISON :" + combinaison.getCombinaisonName() + " RANK :" + combinaison.getRank());


    }

    private void determinateHandRank() {

        if (
                !isRoyalFlush() && !isStraightFlush() && !isFourOfaKind() && !isFullHouse() && !isColor() && !isStraight()
                 && !isThreeofAkind() && !isTwoPair() && !isOnePair()
        ) {

            combinaison = new Combinaison("ONE CARD", 1, 0);
        }
        ;


    }


    private HashMap<Integer, Integer> determinateNumberOfOccurence() {
        HashMap<Integer, Integer> numberOccurence = new HashMap<>();
        int nmbreOccurencemax = 0;


        for (Card card : cardList) {
            if (!numberOccurence.containsKey(card.getCardValue())) {

                numberOccurence.put(card.getCardValue(), 1);
            } else {
                numberOccurence.put(card.getCardValue(), numberOccurence.get(card.getCardValue()) + 1);

            }
            if (numberOccurence.get(card.getCardValue()) > nmbreOccurencemax) {
                nmbreOccurencemax = numberOccurence.get(card.getCardValue());
                valueMaxOccurenced = card.getCardValue();

            }
        }

        return numberOccurence;

    }

    private boolean isStraightFlush() {
        if (isStraight() && isColor()) {
            combinaison.setCombinaisonName("STRAIGHTFLUSH");
            combinaison.setRank(9);
            return true;

        } else return false;


    }

    public boolean isRoyalFlush() {

        if (isColor() && isRoyal()) {
            combinaison.setCombinaisonName("FLUSHROYAL");
            combinaison.setRank(10);
            return true;

        } else return false;
    }

    private boolean isRoyal() {
        boolean isRoyal = false;
        if ((cardList.get(0).getCardValue() == 10) && (cardList.get(1).getCardValue() == 11) && (cardList.get(2).getCardValue() == 12) && (cardList.get(3).getCardValue() == 13) && (cardList.get(4).getCardValue() == 14)) {
            isRoyal = true;
        }
        return isRoyal;

    }

    private boolean isStraight() {
        boolean isStraight = true;
        cardList.sort(Card::compareTo);
        int maxValue = 0;
        for (int position = 0; position < cardList.size() - 1; position++) {
            if (isStraight && cardList.get(position).getCardValue() != cardList.get(position + 1).getCardValue() - 1) {
                isStraight = false;


            }
            if (cardList.get(position).getCardValue() > maxValue) {
                maxValue = cardList.get(position).getCardValue();
            }

        }
        if (isStraight) {
            combinaison = new Combinaison("STRAIGHT", 5, maxValue);
        }
        return isStraight;

    }


    private boolean isColor() {
        boolean isColor = true;
        int maxValue = 0;
        for (int i = 0; i < cardList.size() - 1; i++) {
            if (!cardList.get(i).getCardColor().equals(cardList.get(i + 1).getCardColor())) {

                isColor = false;
            }

        }
        if (isColor) {
            combinaison = new Combinaison("FLUSH", 6, valueMaxOccurenced);
        }
        return isColor;
    }

    private boolean isFourOfaKind() {
        boolean isFourOfaKind = false;
        if (determinateNumberOfOccurence().get(valueMaxOccurenced) == 4) {
            isFourOfaKind = true;
            combinaison = new Combinaison("FOUR OF A KIND", 8, valueMaxOccurenced);

        }
        return isFourOfaKind;

    }


    private boolean isFullHouse() {

        if (isOnePair() && isThreeofAkind()) {
            combinaison.setCombinaisonName("FULL");
            combinaison.setRank(7);
            return true;

        } else return false;

    }

    private boolean isOnePair() {
        boolean isOnepair = false;
        int maxValue = 0;
        int numberOfPair = 0;
        HashMap<Integer, Integer> numberOfOccurences = new HashMap<>(determinateNumberOfOccurence());
        for (Card card : cardList) {
            if (numberOfOccurences.get(card.getCardValue()) != null && numberOfOccurences.get(card.getCardValue()) == 2) {
                numberOfPair++;
                numberOfOccurences.remove(card.getCardValue());
                maxValue = card.getCardValue();
            }

        }
        if (numberOfPair == 1) {
            combinaison = new Combinaison("ONE-PAIR", 2, maxValue);
            isOnepair = true;
        }

        return isOnepair;
    }


    private boolean isTwoPair() {
        boolean isTwoPair = false;
        int maxValue = 0;
        int numberOfPair = 0;
        HashMap<Integer, Integer> numberOfOccurences = new HashMap<>(determinateNumberOfOccurence());
        for (Card card : cardList) {
            if (numberOfOccurences.get(card.getCardValue()) != null && numberOfOccurences.get(card.getCardValue()) == 2) {
                numberOfPair++;
                numberOfOccurences.remove(card.getCardValue());
                if (card.getCardValue() > maxValue) {
                    maxValue = card.getCardValue();
                }
            }

        }
        if (numberOfPair == 2) {
            combinaison = new Combinaison("TWO-PAIR", 3, maxValue);
            isTwoPair = true;
        }
        return isTwoPair;
    }

    private int getNumberOfPair() {
        HashMap<Integer, Integer> numberOfOccurences = new HashMap<>(determinateNumberOfOccurence());

        int numberOfPair = 0;
        for (Card card : cardList) {
            if (numberOfOccurences.get(card.getCardValue()) != null && numberOfOccurences.get(card.getCardValue()) == 2) {
                numberOfPair++;
                numberOfOccurences.remove(card.getCardValue());
            }

        }
        return numberOfPair;
    }

    private boolean isThreeofAkind() {
        boolean IsThreeOfaKind = false;
        if ((determinateNumberOfOccurence().get(valueMaxOccurenced)) == 3) {
            IsThreeOfaKind = true;
            combinaison = new Combinaison("THREE OK A KIND", 4, valueMaxOccurenced);

        }

        return IsThreeOfaKind;


    }


    @Override
    public int compareTo(Object o) {
        HandEvaluator otherHand = (HandEvaluator) o;
        if (combinaison.getRank() != otherHand.combinaison.getRank()) {
            return (combinaison.getRank() - otherHand.combinaison.getRank());


        } else if (combinaison.getHighestValue() != otherHand.combinaison.getHighestValue()) {
            System.out.println("EGALITE DE RANG - COMPARAISON CARTE + FORTE");
            System.out.println("JOUEUR 1 :" + combinaison.getHighestValue() + " JOUEUR 2 :" + otherHand.combinaison.getHighestValue());
            return (combinaison.getHighestValue() - otherHand.combinaison.getHighestValue());
        } else
            return (cardList.get(cardList.size() - 1).getCardValue() - otherHand.cardList.get(otherHand.cardList.size() - 1).getCardValue());
    }

}
